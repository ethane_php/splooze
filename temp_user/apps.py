from django.apps import AppConfig


class TempUserConfig(AppConfig):
    name = 'temp_user'
