from .models import TempUser
from django.contrib.auth import authenticate, login, get_user_model
#from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from common.models import Clubs
import random, string, os
from rest_framework.response import Response
from django.core.mail import send_mail

User = get_user_model()

class TempUserSerializer(serializers.HyperlinkedModelSerializer):
    email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=User.objects.all(), message='Email already registered')])
    password = serializers.CharField(required=False)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=False)
    user_type = serializers.CharField(required=True)
    contact = serializers.CharField(validators=[UniqueValidator(queryset=User.objects.all(), message='Contact already registered')])
    provider_id = serializers.CharField(required=False)
    provider_type = serializers.CharField(required=False)
    avatar = serializers.FileField(use_url=True, required=False)
    gender = serializers.CharField(required=False)
    dob = serializers.DateField(required=False)
    device_id = serializers.CharField(required=True)
    device_type = serializers.CharField(required=True)
    """ address = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    state = serializers.CharField(required=False)
    country = serializers.CharField(required=False)
    message = serializers.CharField(required=False)
    latitude = serializers.CharField(required=False)
    longitude = serializers.CharField(required=False) """
    #user_status = serializers.SerializerMethodField()

    class Meta:
        model = TempUser
        fields = ('temp_id', 'email', 'password', 'contact','first_name', 'last_name', 'avatar','gender','dob', 'user_type', 'provider_id', 'provider_type', 'otp_token','device_id','device_type' )

    def create(self, validated_data):
        if validated_data['user_type'] == 'UR':
            user_token = ''.join(random.choices(string.digits, k=6))        
            validated_data['otp_token']=user_token
            user = TempUser(**validated_data)
            user.save()
            return user
        else :
            club_data = self.context.get('club')
            """ club_data = {}
            club_data['club_name'] = request.data.get('club_name')
            club_data['address_line_1'] = request.data.get('address')
            club_data['city'] = request.data.get('city')
            club_data['state'] = request.data.get('state')
            club_data['country'] = request.data.get('country')
            club_data['message'] = request.data.get('message')
            club_data['latitude'] = request.data.get('latitude')
            club_data['longitude'] = request.data.get('longitude') """
            validated_data['verify_token'] = 'verified'
            userserial = User.objects.create(**validated_data)
            #userserial.is_valid(True)
            password = ''.join(random.choices(string.digits, k=9))
            userserial.set_password(password)
            send_mail('Password from splooze', 'Password: '+password , 'shishir@gmail.com',[userserial.email])
            userserial.save()  
            #club_data['user_id'] = userserial.id
            clubSer = Clubs.objects.create(user_id = userserial, **club_data)
            #clubSer.is_valid(True)
            #clubSer.save()  
            return userserial 

        #os._exit(0)

    def update(self,instance, validated_data):
        tmp_user = TempUser.objects.filter(email=validated_data['email']).order_by('-temp_id')[0:1]        
        #validated_data['otp_token']=user_token
        #user = TempUser(**validated_data)
        #user.save()
        #print(user)
        #os._exit(0)
        return tmp_user[0]
