from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework import permissions
from .serializers import TempUserSerializer
from .models import TempUser
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED
)
from rest_framework.response import Response
from django.core.mail import send_mail
import random, string, os


""" class TempUserViewSet(viewsets.ModelViewSet):
https://medium.com/the-andela-way/creating-a-django-api-using-django-rest-framework-apiview-b365dca53c1d
https://realpython.com/test-driven-development-of-a-django-restful-api/
    queryset = TempUser.objects.all()
    serializer_class = TempUserSerializer
 """
 
@api_view(['GET', 'POST'])
def snippet_list(request):
    
    #List all snippets, or create a new snippet.
    
    if request.method == 'GET':
        snippets = TempUser.objects.all()
        serializer = TempUserSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        club_data = {}
        club_data['club_name'] = request.data.get('club_name')
        club_data['address_line_1'] = request.data.get('address')
        club_data['city'] = request.data.get('city')
        club_data['state'] = request.data.get('state')
        club_data['country'] = request.data.get('country')
        club_data['message'] = request.data.get('message')
        club_data['latitude'] = request.data.get('latitude')
        club_data['longitude'] = request.data.get('longitude')
        serializer = TempUserSerializer(data=request.data, context = context)
        if serializer.is_valid(raise_exception=True):
            serializer.save()            
            if request.data.get('user_type') == 'UR':
                send_mail('opt from splooze', 'Otp: '+serializer.data['otp_token'] , 'shishir@gmail.com',[serializer.data['email']])
                return Response(serializer.data, status=HTTP_201_CREATED)
            else:
                return Response({"message": "Query submited"}, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

""" class TempUserView(APIView):
    def post(self, request):
        serializer = TempUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"success": "Query submited"},HTTP_200_OK) """