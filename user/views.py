from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import permissions, authentication
from .serializers import FollowerSerializer, LikeSerializer
from user_auth.serializers import UserSerializer
from .models import Follower, Like
from django.contrib.auth import get_user_model
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
)
from rest_framework.response import Response
from django.db.models import Q
import os

# Create your models here.
User = get_user_model()

# Create your views here.
@api_view(['GET','POST'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def followers(request): 
    #return Response({'key': 'value'}, status=HTTP_200_OK)
    if request.method == 'GET':
        curr_user_id = request.user.id
        user_id = request.GET.get('user_id', curr_user_id)
        followers = Follower.objects.filter(follower_id = user_id)
        #club_id = getattr(club, 'club_id')
        #snippets = ClubsFab.objects.filter(fab_club_id=club_id)
        serializer = FollowerSerializer(followers, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        curr_user_id = request.user.id
        follow_data = {}
        follow_data['follower_id'] = curr_user_id
        follow_data['following_id'] = request.data.get('follow_id')
        follow = FollowerSerializer(data=follow_data)
        if follow.is_valid(raise_exception=True):
            follow.save()
            return Response(follow.data, status=HTTP_201_CREATED)
        return Response(follow.errors, status=HTTP_400_BAD_REQUEST)

@api_view(['GET', 'DELETE'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def get_delete_follower(request, pk):
    try:
        follow = Follower.objects.get(id=pk)
    except Follower.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)

    # get details of a single follow
    if request.method == 'GET':
        serializer = FollowerSerializer(follow)
        return Response(serializer.data)
    # delete a single follow
    if request.method == 'DELETE':
        follow.delete()
        return Response({"message": "Unfollow successfully."},status=HTTP_200_OK)
        
@api_view(['GET'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def following(request): 
    #return Response({'key': 'value'}, status=HTTP_200_OK)
    if request.method == 'GET':
        curr_user_id = request.user.id
        user_id = request.GET.get('user_id', curr_user_id)
        followers = Follower.objects.filter(following_id = user_id)
        #club_id = getattr(club, 'club_id')
        #snippets = ClubsFab.objects.filter(fab_club_id=club_id)
        serializer = FollowerSerializer(followers, many=True)
        return Response(serializer.data)

@api_view(['GET'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def get_following(request, pk):
    try:
        follow = Follower.objects.get(id=pk)
    except Follower.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)

    # get details of a single follow
    if request.method == 'GET':
        serializer = FollowerSerializer(follow)
        return Response(serializer.data)
        
# Create your views here.
@api_view(['GET','POST'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def likes(request): 
    #return Response({'key': 'value'}, status=HTTP_200_OK)
    if request.method == 'GET':
        curr_user_id = request.user.id
        user_id = request.GET.get('user_id', curr_user_id)
        likes = Like.objects.filter(liked_to_id = user_id)
        #club_id = getattr(club, 'club_id')
        #snippets = ClubsFab.objects.filter(fab_club_id=club_id)
        serializer = LikeSerializer(likes, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        curr_user_id = request.user.id
        like_data = {}
        like_data['liked_by_id'] = curr_user_id
        like_data['liked_to_id'] = request.data.get('like_id')
        like = LikeSerializer(data=like_data)
        if like.is_valid(raise_exception=True):
            like.save()
            return Response(like.data, status=HTTP_201_CREATED)
        return Response(like.errors, status=HTTP_400_BAD_REQUEST)

@api_view(['GET', 'DELETE'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def get_delete_likes(request, pk):
    try:
        likes = Like.objects.get(id=pk)
    except Like.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)

    # get details of a single follow
    if request.method == 'GET':
        serializer = LikeSerializer(follow)
        return Response(serializer.data)
    # delete a single follow
    if request.method == 'DELETE':
        likes.delete()
        return Response({"message": "Unlike successfully."},status=HTTP_200_OK)

# Create your views here.
@api_view(['GET'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def user_profile(request):
    curr_user_id = request.user.id
    user_id = request.GET.get('user_id', curr_user_id)
    user_data = User.objects.get(id = user_id)
    user_seri = UserSerializer(user_data)
    user = user_seri.data
    user['follow_status'] = 0    
    user['follow_id'] = 0
    user['like_status'] = 0  
    user['like_id'] = 0
    follow = Follower.objects.filter(follower_id = curr_user_id, following_id = user_id).first()
    like_to = Like.objects.filter(liked_by_id = curr_user_id, liked_to_id = user_id).first()
    
    if follow:
        user['follow_status'] = 1
        user['follow_id'] = follow.id
    if like_to:
        user['like_status'] = 1
        user['like_id'] = like.id
    user['followers_count'] = Follower.objects.filter(following_id = user_id).count()
    user['follow_count'] = Follower.objects.filter(follower_id = user_id).count()
    user['likes_count'] = Like.objects.filter(liked_to_id = user_id).count()

    return Response(user, status=HTTP_200_OK)


# Create your views here.
@api_view(['GET'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def user_profile_search(request):
    key = request.GET.get('key', '')
    if key =='':
        user_data = User.objects.exclude(is_admin = 1)
    else:
        user_data = User.objects.exclude(is_admin = 1).filter(Q(first_name__contains = key) | Q(last_name__contains = key))

    user_seri = UserSerializer(user_data, many=True)
    user = user_seri.data
    return Response(user, status=HTTP_200_OK)
