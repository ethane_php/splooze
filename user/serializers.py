from .models import Follower, Like, Subscription
#from django.contrib.auth.models import Group
from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.validators import UniqueTogetherValidator

# Create your models here.
User = get_user_model()


class FollowerSerializer(serializers.ModelSerializer):
    follower_user = serializers.SerializerMethodField()
    following_user = serializers.SerializerMethodField()
    follower_id = serializers.IntegerField(required=True)
    following_id = serializers.IntegerField(required=True)


    class Meta:
        model = Follower
        fields = ('id','follower_id', 'following_id', 'follower_user', 'following_user')
        validators = [
            UniqueTogetherValidator(
                queryset=Follower.objects.all(),
                fields=('follower_id', 'following_id'),
                message='Allready following'
            )
        ]

    def get_follower_user(self, obj):
        user = User.objects.get(id = obj.follower_id)
        user_data = {}
        user_data['id'] = user.id
        user_data['avatar'] = user.avatar.url if user.avatar else ''
        user_data['first_name'] = user.first_name
        user_data['last_name'] = user.last_name

        return user_data

    def get_following_user(self, obj):
        user = User.objects.get(id = obj.following_id)
        user_data = {}
        user_data['id'] = user.id
        user_data['avatar'] = user.avatar.url if user.avatar else ''
        user_data['first_name'] = user.first_name
        user_data['last_name'] = user.last_name
        return user_data

class LikeSerializer(serializers.ModelSerializer):
    liked_by_user = serializers.SerializerMethodField()
    liked_to_user = serializers.SerializerMethodField()
    liked_by_id = serializers.IntegerField(required=True)
    liked_to_id = serializers.IntegerField(required=True)


    class Meta:
        model = Like
        fields = ('id','liked_by_id', 'liked_to_id', 'liked_by_user', 'liked_to_user')
        validators = [
            UniqueTogetherValidator(
                queryset=Like.objects.all(),
                fields=('liked_by_id', 'liked_to_id'),
                message='Allready liked'
            )
        ]

    def get_liked_by_user(self, obj):
        user = User.objects.get(id = obj.liked_by_id)
        user_data = {}
        user_data['id'] = user.id
        user_data['avatar'] = user.avatar.url if user.avatar else ''
        user_data['first_name'] = user.first_name
        user_data['last_name'] = user.last_name

        return user_data

    def get_liked_to_user(self, obj):
        user = User.objects.get(id = obj.liked_to_id)
        user_data = {}
        user_data['id'] = user.id
        user_data['avatar'] = user.avatar.url if user.avatar else ''
        user_data['first_name'] = user.first_name
        user_data['last_name'] = user.last_name
        return user_data


class SubsSerializer(serializers.ModelSerializer):
    sub_amount = serializers.DecimalField(max_digits=16, decimal_places=8)
    sub_start_at = serializers.DateTimeField(required=True)
    sub_end_at = serializers.DateTimeField(required=True)
    sub_transaction_id = serializers.IntegerField(required=True)
    sub_plan_id = serializers.IntegerField(required=True)
    sub_user_id = serializers.IntegerField(required=True)

    class Meta:
        model = Subscription
        fields = ('sub_id','sub_amount', 'sub_start_at', 'sub_end_at', 'sub_transaction_id', 'sub_plan_id','sub_user_id')