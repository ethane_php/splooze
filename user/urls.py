from django.urls import path, include
from . import views as user_views

#router = routers.DefaultRouter()
#router.register(r'temp_user', temp_user_views.TempUserViewSet)

urlpatterns = [
    path('user/follow/',user_views.followers),
    path('user/follow/<int:pk>',user_views.get_delete_follower),
    path('user/following/',user_views.following),
    path('user/following/<int:pk>',user_views.get_following),
    path('user/like/',user_views.likes),
    path('user/like/<int:pk>',user_views.get_delete_likes),
    path('user/profile/', user_views.user_profile),
    path('user/profile/search/', user_views.user_profile_search)
]

