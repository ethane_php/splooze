# Generated by Django 2.2 on 2019-05-24 06:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0015_delete_subscription'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('user', '0003_auto_20190523_1613'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('sub_id', models.AutoField(primary_key=True, serialize=False)),
                ('sub_amount', models.DecimalField(decimal_places=8, max_digits=16)),
                ('sub_start_at', models.DateTimeField()),
                ('sub_end_at', models.DateTimeField()),
                ('sub_plan', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sub_plans', to='common.SubscriptionPlans')),
                ('sub_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sub_plan_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
