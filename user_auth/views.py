from django.shortcuts import render
from django.contrib.auth import authenticate, login, get_user_model
from .serializers import UserSerializer
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework import permissions
from rest_framework.response import Response
from django.core.mail import send_mail
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_401_UNAUTHORIZED,
    HTTP_406_NOT_ACCEPTABLE
)
import random, string ,os
from .send_otp import send_sms

User = get_user_model()

@api_view(['POST'])
def verify_user(request):    
    email_or_contact = request.POST['email_or_contact']
    password = request.POST['password']

    
    user = authenticate(request, username=email_or_contact, password=password)
    if user is not None:
        serializer = UserSerializer(user)
        if user.verify_token == 'verified':
            if user.is_active:
                token, _  = Token.objects.get_or_create(user=user)
                # data['key'] = token
                return Response({'token': token.key, 'user':serializer.data}, status=HTTP_200_OK)
                # Redirect to a success page.
            else:
                return Response({'message': 'Account is not active.'}, status=HTTP_400_BAD_REQUEST)
        else:
            user_token = ''.join(random.choices(string.digits, k=6))        
            user.otp_token = user_token
            user.save()
            #send_sms(user_token,user.contact)
            send_mail('opt from splooze', 'Otp: '+user_token , 'shishir@gmail.com',[serializer.data['email']])
            return Response({'message': 'Otp not verified.', 'user': serializer.data}, status=HTTP_200_OK)
    else:
        # Return an 'invalid login' error message.
        return Response({'message': 'No active account found with the given credentials.'}, status=HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def register_user(request):  
    club_data = {}
    club_data['club_name'] = request.data.get('club_name')
    club_data['address_line_1'] = request.data.get('address')
    club_data['city'] = request.data.get('city')
    club_data['state'] = request.data.get('state')
    club_data['country'] = request.data.get('country')
    club_data['message'] = request.data.get('message')
    club_data['latitude'] = request.data.get('latitude')
    club_data['longitude'] = request.data.get('longitude')
    user_pass = request.data.get('password')
    context = {"club": club_data, "password": user_pass}
    serializer = UserSerializer(data=request.data, context = context)
    if serializer.is_valid(raise_exception=True):
        serializer.save()            
        if request.data.get('user_type') == 'UR':
            send_mail('opt from splooze', 'Otp: '+serializer.data['otp_token'] , 'shishir@gmail.com',[serializer.data['email']])
            return Response(serializer.data, status=HTTP_201_CREATED)
        else:
            return Response({"message": "Query submited."}, status=HTTP_200_OK)
    return Response(serializer.errors, status=HTTP_406_NOT_ACCEPTABLE)
