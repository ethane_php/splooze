from django.shortcuts import render
from django.contrib.auth import authenticate, login, get_user_model
from .serializers import UserSerializer
from temp_user.serializers import TempUserSerializer
from temp_user.models import TempUser
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework import permissions
from django.core.mail import send_mail
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED
)
from rest_framework.response import Response
import random, string ,os

User = get_user_model()

@api_view(['POST'])

def verify_otp(request):
    user = User.objects.get(id=request.POST['id'])
    otp = request.POST['otp']
    if user.otp_token == otp :
        user.verify_token = 'verified'
        user.is_active = True
        user.save()  
        token, _  = Token.objects.get_or_create(user=user)
        serializer = UserSerializer(user)
           
        return Response({'token': token.key, 'user':serializer.data}, status=HTTP_201_CREATED)
    else:
        return Response({"message": "OTP not verified"}, status=HTTP_200_OK)

@api_view(['POST'])
def resend_otp(request):
    user = User.objects.get(id=request.POST['id'])
    user_token = ''.join(random.choices(string.digits, k=6))        
    user.otp_token = user_token
    user.save()
    serializer = UserSerializer(user)
    send_mail('opt from splooze', 'Otp: '+user_token , 'shishir@gmail.com',[user.email])
    return Response({"message": "OTP send",'user':serializer.data}, status=HTTP_200_OK)

