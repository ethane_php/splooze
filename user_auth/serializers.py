from django.contrib.auth import get_user_model
#from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from common.models import Clubs
from common.serializers import ClubSerializer
from django.core.mail import send_mail
import datetime, random, string, os


User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    
    email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=User.objects.all(), message='Email already registered')])
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=False)
    user_type = serializers.CharField(required=True)
    contact = serializers.CharField(validators=[UniqueValidator(queryset=User.objects.all(), message='Contact already registered')])
    provider_id = serializers.CharField(required=False)
    provider_type = serializers.CharField(required=False)
    user_status = serializers.SerializerMethodField()
    avatar = serializers.FileField(use_url=True, required=False)
    gender = serializers.CharField(required=False)
    dob = serializers.DateField(required=False, input_formats=["%d-%m-%Y"])
    device_id = serializers.CharField(required=True)
    device_type = serializers.CharField(required=True)
    clubs = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'email', 'contact','first_name', 'last_name', 'user_type', 'gender', 'is_active', 'dob','provider_id', 'provider_type', 'otp_token', 'user_added_at', 'user_last_updated_at', 'verify_token', 'avatar', 'user_status', 'device_id','device_type', 'clubs' )
        extra_kwargs = {
            'ex_key': {'view_name': 'test'}
        }

    def create(self, validated_data):
        if validated_data['user_type'] == 'UR':
            user_token = ''.join(random.choices(string.digits, k=6))        
            validated_data['otp_token']=user_token             
            user = User(**validated_data)  
            user.set_password(self.context.get('password'))
            user.save()
            return user
        else :
            club_data = self.context.get('club')
            """ club_data = {}
            club_data['club_name'] = request.data.get('club_name')
            club_data['address_line_1'] = request.data.get('address')
            club_data['city'] = request.data.get('city')
            club_data['state'] = request.data.get('state')
            club_data['country'] = request.data.get('country')
            club_data['message'] = request.data.get('message')
            club_data['latitude'] = request.data.get('latitude')
            club_data['longitude'] = request.data.get('longitude') """
            validated_data['verify_token'] = 'verified'
            userserial = User.objects.create(**validated_data)
            #userserial.is_valid(True)
            password = ''.join(random.choices(string.digits, k=9))
            userserial.set_password(password)
            send_mail('Password from splooze', 'Password: '+password , 'shishir@gmail.com',[userserial.email])
            userserial.save()  
            #club_data['user_id'] = userserial.id
            clubSer = Clubs.objects.create(user_id = userserial, **club_data)
            #clubSer.is_valid(True)
            #clubSer.save()  
            return userserial 



    def get_user_status(self, obj):
        if obj.is_active and obj.verify_token =="verified" and obj.password !="":
            return "complete"
        elif obj.is_active and obj.verify_token =="" and obj.password == "":     
            return "incomplete"
        else:
            return "not_verified"

    def get_clubs(self, obj):
            if obj.user_type =="CR":
                club_data = Clubs.objects.get(user_id_id = obj.id)  
                club ={}              
                club['club_id'] = club_data.club_id      
                club['club_name'] = club_data.club_name
                club['address_line_1'] = club_data.address_line_1
                club['city'] = club_data.city
                club['state'] = club_data.state
                club['country'] = club_data.country
                club['message'] = club_data.message
                club['latitude'] = club_data.latitude
                club['longitude'] = club_data.longitude
                return club
            else:
                return []
