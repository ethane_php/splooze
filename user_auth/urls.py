from django.urls import path, include
from rest_framework import routers
from . import views as user_views
from . import otp_views as otp

#router = routers.DefaultRouter()
#router.register(r'user', user_views.UserViewSet)
#router.register(r'groups', user_views.GroupViewSet)

urlpatterns = [
    #path('', include(router.urls)),
    path('login/', user_views.verify_user, name='verify-user'),
    path('user/register/', user_views.register_user, name='register-user'),
    path('user/verify_otp/', otp.verify_otp, name='verify-otp'),
    path('user/resend_otp/', otp.resend_otp, name='resend-otp'),
]

