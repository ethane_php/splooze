from .models import User
from rest_framework import authentication
from rest_framework import exceptions
from django.db.models import Q


class EmailAuthBackend(object):

    def get_user(self, user_id):
       try:
          return User.objects.get(pk=user_id)
       except User.DoesNotExist:
          return None


    def authenticate(self, request, username=None, password=None):
        try:
            user = User.objects.get(email=username)
        except User.DoesNotExist:
            return None

        return user if user.check_password(password) else None

class PhoneAuthBackend(object):

    def get_user(self, user_id):
       try:
          return User.objects.get(pk=user_id)
       except User.DoesNotExist:
          return None


    def authenticate(self, request, username=None, password=None):
        try:
            user = User.objects.get(contact=username)
        except User.DoesNotExist:
            return None

        return user if user.check_password(password) else None