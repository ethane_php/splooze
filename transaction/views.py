from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import permissions, authentication
from .serializers import TransactionSerializer
from user.serializers import SubsSerializer
from user.models import Subscription
from .models import AllUser
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
)
from rest_framework.response import Response
from django.utils import timezone
from django.conf import settings
import os


@api_view(['GET','POST'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def subscription_transaction(request): 
    if request.method == 'GET':
        trans = AllUser.objects.all()
        serializer = TransactionSerializer(trans, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        user_id = request.user.id
        trans_data = {}
        trans_data['transaction_user_id'] = user_id
        trans_data['transaction_hash'] = request.data.get('transaction_hash')
        trans_data['transaction_status'] = request.data.get('transaction_status')
        trans_data['transaction_type'] = 'subscription'
        trans_data['transaction_amount'] = request.data.get('transaction_amount')
        trans = TransactionSerializer(data=trans_data)
        if trans.is_valid(raise_exception=True):
            trans.save()  
            if trans.data['transaction_id'] != '':
                sub = {}
                sub['sub_amount'] = request.data.get('transaction_amount')
                sub['sub_start_at'] = (timezone.now()).strftime(settings.DATETIME_FORMAT)
                sub['sub_end_at'] = (timezone.now() + timezone.timedelta(days=30)).strftime(settings.DATETIME_FORMAT)
                sub['sub_transaction_id'] = trans.data['transaction_id']
                sub['sub_plan_id'] = request.data.get('subscription_id')
                sub['sub_user_id'] = user_id
                subs = SubsSerializer(data=sub)
                if subs.is_valid(raise_exception=True):
                    subs.save()
            return Response(trans.data, status=HTTP_201_CREATED)
        return Response(trans.errors, status=HTTP_400_BAD_REQUEST)