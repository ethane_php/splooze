from .models import AllUser
#from django.contrib.auth.models import Group
from rest_framework import serializers
from user_auth.serializers import UserSerializer
from django.contrib.auth import get_user_model
import datetime

# Create your models here.
User = get_user_model()


class TransactionSerializer(serializers.ModelSerializer):
    transaction_by_user = serializers.SerializerMethodField()

    transaction_hash = serializers.CharField(required=True)
    transaction_user_id = serializers.IntegerField( required=True)
    transaction_status = serializers.CharField(required=True)
    transaction_type = serializers.CharField(required=True)
    transaction_amount = serializers.DecimalField(max_digits=16, decimal_places=8)
    transaction_ts = serializers.SerializerMethodField()


    class Meta:
        model = AllUser
        fields = ('transaction_id','transaction_hash','transaction_user_id', 'transaction_by_user', 'transaction_status', 'transaction_type', 'transaction_amount','transaction_date', 'transaction_ts')
        read_only_fields = ['transaction_id','transaction_date','transaction_by_user']

    def get_transaction_by_user(self, obj):
        user_data = User.objects.get(id=obj.transaction_user_id)
        userSeri = UserSerializer(user_data)
        return userSeri.data

    def get_transaction_ts(self, obj):
        return datetime.datetime.timestamp(obj.transaction_date)

