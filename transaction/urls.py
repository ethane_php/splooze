from django.urls import path
from . import views as views

urlpatterns = [
    path('transaction/subscription/',views.subscription_transaction)
]