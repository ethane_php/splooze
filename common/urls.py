from django.urls import path
from rest_framework import routers
from . import fab_views as fab_views
from . import views as views

urlpatterns = [
    path('fab/<int:pk>',fab_views.get_delete_update_club_fab),
    path('fab/',fab_views.clubs_fab),
    path('category/',views.event_category),
    path('subs_plans/', views.subscription_plan),
]