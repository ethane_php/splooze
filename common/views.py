from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import permissions, authentication
from .serializers import CategorySerializer, SubsPlansSerializer
from .models import EventCategory, SubscriptionPlans
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
)
from rest_framework.response import Response
import os

@api_view(['GET'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def event_category(request): 
    snippets = EventCategory.objects.all()
    serializer = CategorySerializer(snippets, many=True)
    return Response(serializer.data)

@api_view(['GET'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def subscription_plan(request): 
    snippets = SubscriptionPlans.objects.all()
    serializer = SubsPlansSerializer(snippets, many=True)
    return Response(serializer.data)