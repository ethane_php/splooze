from .models import Clubs, EventCategory, ClubsFab, Venues, SubscriptionPlans
#from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
import random, string, os

      
class SubsPlansSerializer(serializers.ModelSerializer):
    plan_id = serializers.IntegerField(required=True)
    plan_name = serializers.CharField(required=True)
    plan_price = serializers.DecimalField(max_digits=16, decimal_places=8)

    class Meta:
        model = SubscriptionPlans
        fields = ('plan_id', 'plan_name', 'plan_price' )

class ClubSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(required=True)
    club_name = serializers.CharField(required=True)
    address_line_1 = serializers.CharField(required=False)
    city = serializers.CharField(required=True)
    state = serializers.CharField(required=True)
    country = serializers.CharField(required=True)
    message = serializers.CharField(required=True)
    latitude = serializers.CharField(required=True)
    longitude = serializers.CharField(required=True)


    class Meta:
        model = Clubs
        fields = ('club_id', 'user_id', 'club_name', 'address_line_1','city', 'state', 'country', 'message', 'latitude', 'longitude' )
       
class VenueSerializer(serializers.ModelSerializer):
    venue_name = serializers.CharField(required=True)
    address_line_1 = serializers.CharField(required=False)
    city = serializers.CharField(required=True)
    state = serializers.CharField(required=True)
    country = serializers.CharField(required=True)
    latitude = serializers.CharField(required=True)
    longitude = serializers.CharField(required=True)


    class Meta:
        model = Venues
        fields = ('venue_id', 'venue_name', 'address_line_1','city', 'state', 'country', 'latitude', 'longitude' )
       

       
class CategorySerializer(serializers.ModelSerializer):
    category_id = serializers.IntegerField(required=True)
    category_title = serializers.CharField(required=True)

    class Meta:
        model = EventCategory
        fields = ('category_id', 'category_title' )
       

class ClubsFabSerializer(serializers.HyperlinkedModelSerializer):
    fab_club_id = serializers.IntegerField(required=True)
    fab_name = serializers.CharField(required=True)
    fab_price = serializers.DecimalField(max_digits=16, decimal_places=8)
    fab_image = serializers.ImageField( required=False)

    class Meta:
        model = ClubsFab
        fields = ('fab_id','fab_club_id', 'fab_name', 'fab_price', 'fab_image' )


class AttendeeStatusSerializer(serializers.ModelSerializer):
    as_id = serializers.IntegerField(required=True)
    as_title = serializers.CharField(required=True)

    class Meta:
        model = SubscriptionPlans
        fields = ('as_id', 'as_title' )