from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from rest_framework.views import exception_handler
from rest_framework.exceptions import ErrorDetail
import os

User = get_user_model()

def validate_email_exist(value):
    try:
        user = User.objects.get(email=username)
        raise ValidationError("Email already registered")
    except User.DoesNotExist:
        return value
    

def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    
    # Now add the HTTP status code to the response.
    if response is not None:
        #print(response.data)
        #os._exit(0)
        for error_key in response.data:
            if error_key == 'detail':
                message = response.data[error_key]
            else:
                message = response.data[error_key][0]
            break
        response.data = {'message': message.replace('This field', error_key.replace('_', ' '))}

    return response