# Generated by Django 2.2 on 2019-04-30 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_auto_20190429_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='clubs',
            name='city',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='clubs',
            name='country',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='clubs',
            name='state',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
