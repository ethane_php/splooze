from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import permissions, authentication
from .serializers import ClubsFabSerializer, ClubSerializer
from .models import ClubsFab, Clubs
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
)
from rest_framework.response import Response
import os

# Create your views here.
@api_view(['GET','POST'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def clubs_fab(request): 
    #return Response({'key': 'value'}, status=HTTP_200_OK)
    if request.method == 'GET':
        user_id = request.user.id
        club = Clubs.objects.filter(user_id_id = user_id).first()
        club_id = getattr(club, 'club_id')
        snippets = ClubsFab.objects.filter(fab_club_id=club_id)
        serializer = ClubsFabSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        user_id = request.user.id
        club = Clubs.objects.filter(user_id_id = user_id).first()
        club_fab = ClubsFabSerializer(data=request.data)
        if club_fab.is_valid(raise_exception=True):
            club_fab.save()       
            return Response(club_fab.data, status=HTTP_201_CREATED)
        return Response(club_fab.errors, status=HTTP_400_BAD_REQUEST)

@api_view(['GET', 'DELETE', 'PUT'])
def get_delete_update_club_fab(request, pk):
    try:
        clubs_fab = ClubsFab.objects.get(fab_id=pk)
    except ClubsFab.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)

    # get details of a single fab
    if request.method == 'GET':
        serializer = ClubsFabSerializer(clubs_fab)
        return Response(serializer.data)
    # update details of a single fab
    if request.method == 'PUT':
        serializer = ClubsFabSerializer(clubs_fab, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    # delete a single fab
    if request.method == 'DELETE':
        clubs_fab.delete()
        return Response({"message": "Beverage deleted successfully."},status=HTTP_200_OK)
        