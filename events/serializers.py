from .models import Events, Fabs, Attendee
from common.serializers import CategorySerializer, ClubsFabSerializer, VenueSerializer, AttendeeStatusSerializer
from common.models import EventCategory, Venues, ClubsFab
from django.conf import settings
from haversine import haversine
from django.contrib.auth import authenticate, login, get_user_model
from user_auth.serializers import UserSerializer
#from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.validators import UniqueValidator, UniqueTogetherValidator
#from common.models import Clubs
import os
from rest_framework.response import Response

User = get_user_model()

class EventsSerializer(serializers.HyperlinkedModelSerializer):
    category = serializers.SerializerMethodField()
    venue_distance = serializers.SerializerMethodField()
    venue = VenueSerializer(allow_null=True, read_only=True, source='event_venue')
    event_user = serializers.SerializerMethodField()
    joined_user = serializers.SerializerMethodField()
    event_fabs = serializers.SerializerMethodField()
    CHOICE_OPTOINS = (
        ('Y', 'Yes'),
        ('N', 'No'),
    )
    event_category_id = serializers.IntegerField(required = True)
    event_owner_id = serializers.IntegerField( required=True)
    event_venue_id = serializers.IntegerField(required=True)
    event_title = serializers.CharField(required=True)
    event_dsc = serializers.CharField(required=False)
    event_img = serializers.FileField(required=False)
    event_start_at = serializers.DateTimeField(format=settings.DATETIME_FORMAT)
    event_is_premium = serializers.ChoiceField(allow_blank =True, choices=CHOICE_OPTOINS)
    event_total_seat = serializers.IntegerField(required=False)
    event_availed_seat = serializers.IntegerField(required=False)
    event_fees = serializers.DecimalField(max_digits=16, decimal_places=8)
    event_has_fab = serializers.ChoiceField(allow_blank =True, choices=CHOICE_OPTOINS)
    event_status = serializers.BooleanField(default=False)

    class Meta:
        model = Events
        fields = ('event_id', 'event_category_id', 'category', 'event_owner_id', 'event_user', 'event_venue_id', 'venue', 'venue_distance', 'event_title', 'event_dsc', 'event_img', 'event_start_at', 'event_is_premium', 'event_total_seat', 'event_availed_seat', 'event_fees', 'event_has_fab', 'event_status', 'joined_user', 'event_fabs' )

    def create(self, validated_data):
        event = Events(**validated_data)
        #print(validated_data)
        #os._exit(0)
        event.save()
        return event

    def update(self,instance, validated_data):
        event = super().update(instance, validated_data)
        return event
    
    def get_category(self, event):
        cat = EventCategory.objects.get(category_id=event.event_category_id)
        catSeri = CategorySerializer(cat)
        return catSeri.data

    def get_venue_distance(self, event):
        venue = Venues.objects.get(venue_id=event.event_venue_id)
        lat = float(self.context.get('lat'))
        lng = float(self.context.get('lng'))  
        lat2 = float(venue.latitude)
        lng2 = float(venue.longitude)     
        distance = haversine((lat, lng), (lat2, lng2))
        return distance   

    def get_event_user(self, event):
        user_data = UserSerializer(User.objects.get(id=event.event_owner_id))
        return user_data.data

    def get_event_status(self, event):
        if event.event_status:
            return 'Y'
        else:
            return 'N'
    
    def get_joined_user(self, event):
        try:
            attendees = Attendee.objects.filter(event_id=event.event_id)
            attend_data = AttendeeSerializer(attendees, many=True)
            return attend_data.data
        except Attendee.DoesNotExist:
            return ''
        return 

    def get_event_fabs(self, event):
        fabs = Fabs.objects.filter(event_id=event.event_id)
        fabs_list = EventFabsSerializer(fabs, many=True)
        return fabs_list.data 
""" 
    def to_internal_value(self, data):
        if self.event_category_id is not None:
            data = self.event_category_id.to_internal_value(data)
        try:
            self.get_queryset().get(pk=data)
        except ObjectDoesNotExist:
            self.fail('does_not_exist', pk_value=data)
        except (TypeError, ValueError):
            self.fail('incorrect_type', data_type=type(data).__name__)
        else:
            return data """

class EventFabsSerializer(serializers.HyperlinkedModelSerializer):
    fab_set = ClubsFabSerializer(allow_null=True, read_only=True, source='fab')

    event_id = serializers.IntegerField(required=True)
    fab_id = serializers.IntegerField(required=True)
    event_fab_price = serializers.DecimalField(max_digits=16, decimal_places=8, required=True)

    class Meta:
        model = Fabs
        fields = ('event_fab_id', 'event_id', 'fab_set', 'fab_id', 'event_fab_price')

    
class AttendeeSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(allow_null=True, read_only=True, source='user')
    event_id = serializers.IntegerField(required=True)
    user_id = serializers.IntegerField(required=True)
    attendee_status_id = serializers.IntegerField(required=True)
    attendee_status_data = AttendeeStatusSerializer(allow_null=True, read_only=True, source='attendee_status')

    class Meta:
        model = Attendee
        fields = ('attendee_id', 'event_id', 'user_id', 'user_data', 'attendee_status_id', 'attendee_status_data')
        validators = [
            UniqueTogetherValidator(
                queryset=Attendee.objects.all(),
                fields=('event_id', 'user_id'),
                message='Allready updated status'
            )
        ]