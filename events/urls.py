from django.urls import path, include
from rest_framework import routers
from . import views as event_views

#router = routers.DefaultRouter()
#router.register(r'temp_user', temp_user_views.TempUserViewSet)

urlpatterns = [
    path('events/',event_views.user_events),
    path('events/<int:pk>',event_views.get_delete_update_event),
    path('events/rspv/', event_views.user_attendee),
    path('events/rspv/<int:pk>', event_views.update_user_attendee),
    path('events/search/', event_views.search_event)
]

