# Generated by Django 2.2 on 2019-05-22 06:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0007_auto_20190521_1916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='events',
            name='event_img',
            field=models.ImageField(blank=True, max_length=255, upload_to='event_image/'),
        ),
    ]
