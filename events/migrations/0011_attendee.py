# Generated by Django 2.2 on 2019-05-27 09:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0015_delete_subscription'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('events', '0010_auto_20190522_1341'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attendee',
            fields=[
                ('attendee_id', models.BigAutoField(primary_key=True, serialize=False)),
                ('attendee_status', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='common.AttendeeStatus')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='events', to='events.Events')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
