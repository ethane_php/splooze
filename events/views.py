from django.shortcuts import render
from django.db.models import F
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework import permissions, authentication
from .serializers import EventsSerializer, EventFabsSerializer, AttendeeSerializer
from common.serializers import VenueSerializer
from common.models import ClubsFab, Venues
from .models import Events, Attendee
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
)
from rest_framework.response import Response
import os

# Create your views here.
@api_view(['GET','POST'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def user_events(request): 
    #return Response({'key': 'value'}, status=HTTP_200_OK)
    if request.method == 'GET':
        user_id = request.user.id
        snippets = Events.objects.filter(event_owner_id = user_id).order_by('-event_id')
        serializer = EventsSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        user_id = request.user.id
        venue = {}
        venue['venue_name'] = request.data.get('venue_name')
        venue['address_line_1'] = request.data.get('address_line_1')
        venue['city'] = request.data.get('city')
        venue['state'] = request.data.get('state')
        venue['country'] = request.data.get('country')
        venue['latitude'] = request.data.get('latitude')
        venue['longitude'] = request.data.get('longitude')
        event = {}
        event['event_title'] = request.data.get('event_title')
        event['event_dsc'] = request.data.get('event_dsc')
        event['event_img'] = request.data.get('event_img')
        event['event_start_at'] = request.data.get('event_start_at')
        event['event_is_premium'] = 'N' if request.data.get('event_is_premium') == '' else request.data.get('event_is_premium')
        event['event_total_seat'] = 0 if request.data.get('event_total_seat')=='' else request.data.get('event_total_seat')
        event['event_fees'] = 0 if request.data.get('event_fees')=='' else request.data.get('event_fees')
        event['event_has_fab'] = 'N' if request.data.get('event_has_fab') == '' else request.data.get('event_has_fab')
        event['event_category_id'] = request.data.get('event_category_id')
        event['event_fab_ids'] = request.POST.getlist('event_fab_ids[]')
        event['event_owner_id'] = user_id
        #print(request.POST.getlist('event_fab_ids[]'))
        #os._exit(0)
        venue_seri = VenueSerializer(data=request.data)
        if venue_seri.is_valid(raise_exception=True):
            venue_seri.save()       
            if venue_seri.data['venue_id']:
                event['event_venue_id'] = venue_seri.data['venue_id']
                serializer = EventsSerializer(data = event, context={'request':request})
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    if serializer.data['event_id']!='' and request.data.get('event_has_fab') == "Y":
                        for fab_id in event['event_fab_ids']:
                            fab_data = ClubsFab.objects.get(fab_id = fab_id)
                            event_fab = {}
                            event_fab['event_id'] = serializer.data['event_id']
                            event_fab['fab_id'] = int(fab_id)
                            event_fab['event_fab_price'] = fab_data.fab_price
                            fabseri = EventFabsSerializer(data = event_fab, context={'request':request})
                            if fabseri.is_valid(raise_exception=True):
                                fabseri.save()
                    event_data = Events.objects.get(event_id = serializer.data['event_id'])
                    serial = EventsSerializer(instance=event_data)
                    return Response(serial.data, status=HTTP_201_CREATED)            
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

@api_view(['GET', 'DELETE', 'PUT'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def get_delete_update_event(request, pk):
    user_id = request.user.id
    try:
        event = Events.objects.get(event_id=pk)
    except Events.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)
    
    if request.method == 'GET':
        serializer = EventsSerializer(event)
        return Response(serializer.data)
    # update details of a single fab
    if request.method == 'PUT':
        venue = Venues.objects.get(venue_id = event.event_venue_id)
        venue_data = {}
        venue_data['venue_name'] = request.data.get('venue_name')
        venue_data['address_line_1'] = request.data.get('address_line_1')
        venue_data['city'] = request.data.get('city')
        venue_data['state'] = request.data.get('state')
        venue_data['country'] = request.data.get('country')
        venue_data['latitude'] = request.data.get('latitude')
        venue_data['longitude'] = request.data.get('longitude')
        venue_serializer = VenueSerializer(venue, data=venue_data)
        if venue_serializer.is_valid(raise_exception=True):
            venue_serializer.save()

        event_data = {}
        event_data['event_title'] = request.data.get('event_title')
        event_data['event_dsc'] = request.data.get('event_dsc')        
        event_data['event_start_at'] = request.data.get('event_start_at')
        event_data['event_is_premium'] = 'N' if request.data.get('event_is_premium') == '' else request.data.get('event_is_premium')
        event_data['event_total_seat'] = 0 if request.data.get('event_total_seat')=='' else request.data.get('event_total_seat')
        event_data['event_fees'] = 0 if request.data.get('event_fees')=='' else request.data.get('event_fees')
        event_data['event_has_fab'] = 'N' if request.data.get('event_has_fab') == '' else request.data.get('event_has_fab')
        event_data['event_category_id'] = request.data.get('event_category_id')
        #event_data['event_fab_ids'] = request.POST.getlist('event_fab_ids[]')
        event_data['event_venue_id'] = event.event_venue_id
        event_data['event_owner_id'] = user_id
        if request.data.get('event_img'):
            event_data['event_img'] = request.data.get('event_img')
          
        serializer = EventsSerializer(event, data=event_data)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    # delete a single fab
    if request.method == 'DELETE':
        event.delete()
        return Response({"message": "Event deleted successfully."},status=HTTP_200_OK)
        

@api_view(['POST'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def user_attendee(request): 
    
    user_id = request.user.id
    attendee_data ={}
    attendee_data['event_id'] = request.data.get('event_id')
    attendee_data['user_id'] = user_id
    attendee_data['attendee_status_id'] = request.data.get('status')
    serializer = AttendeeSerializer(data=attendee_data)
    if serializer.is_valid(raise_exception=True):
        serializer.save()            
        return Response(serializer.data, status=HTTP_201_CREATED) 

@api_view(['PUT'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def update_user_attendee(request, pk):
    user_id = request.user.id
    try:
        attendee_data = Attendee.objects.get(attendee_id=pk)
    except Attendee.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)
    attendee_user_data = {}
    attendee_user_data['event_id'] = request.data.get('event_id')
    attendee_user_data['user_id'] = user_id
    attendee_user_data['attendee_status_id'] = request.data.get('status')
    serializer = AttendeeSerializer(attendee_data, data=attendee_user_data)
    if serializer.is_valid(raise_exception=True):
        serializer.save()
        return Response(serializer.data, status=HTTP_200_OK)
    return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    

@api_view(['GET'])
@authentication_classes((authentication.TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
def search_event(request):
    radius_km = int(request.GET.get('radius'))
    search_type = request.GET.get('search_type')
    limit = 100
    lat = request.GET.get('lat')
    lng = request.GET.get('long')
    if search_type == 'all':
        query = """SELECT event_id, (6367*acos(cos(radians(%2f))
                *cos(radians(latitude))*cos(radians(longitude)-radians(%2f))
                +sin(radians(%2f))*sin(radians(latitude))))
                AS distance FROM events_events as e left join common_venues as v on e.event_venue_id = v.venue_id HAVING
                distance < %2f ORDER BY distance ASC LIMIT 0, %d""" % (
            float(lat),
            float(lng),
            float(lat),
            radius_km,
            limit )
    elif search_type == 'prime':
        query = """SELECT event_id, (6367*acos(cos(radians(%2f))
                *cos(radians(latitude))*cos(radians(longitude)-radians(%2f))
                +sin(radians(%2f))*sin(radians(latitude))))
                AS distance FROM events_events as e left join common_venues as v on e.event_venue_id = v.venue_id WHERE event_is_premium='Y' HAVING
                distance < %2f ORDER BY distance ASC LIMIT 0, %d""" % (
            float(lat),
            float(lng),
            float(lat),
            radius_km,
            limit )
    elif search_type == 'category':
        cat_id = int(request.GET.get('cat_id'))
        query = """SELECT event_id, (6367*acos(cos(radians(%2f))
                *cos(radians(latitude))*cos(radians(longitude)-radians(%2f))
                +sin(radians(%2f))*sin(radians(latitude))))
                AS distance FROM events_events as e left join common_venues as v on e.event_venue_id = v.venue_id WHERE event_category_id=%d HAVING
                distance < %2f ORDER BY distance ASC LIMIT 0, %d""" % (
            float(lat),
            float(lng),
            float(lat),
            cat_id,
            radius_km,
            limit )
            
    queryset = Events.objects.raw(query)
    serializer = EventsSerializer(queryset, context={'lat': lat, 'lng': lng}, many=True)
    return Response(serializer.data, status=HTTP_200_OK)
